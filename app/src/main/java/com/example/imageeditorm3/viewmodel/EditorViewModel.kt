package com.example.imageeditorm3.viewmodel

import android.view.MotionEvent
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Path
import androidx.lifecycle.ViewModel
import com.example.imageeditorm3.model.data.EditorEvent
import com.example.imageeditorm3.model.data.EditorObject
import com.example.imageeditorm3.model.data.EditorState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import java.util.UUID
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

class EditorViewModel : ViewModel() {
    private val _uiState = MutableStateFlow(EditorState())
    val uiState = _uiState.asStateFlow()

    private val brushPath =
        (_uiState.value.currentDrawingPath) as EditorObject.BrushPath

    fun handleEvent(contentEvent: EditorEvent) {
        when (contentEvent) {
            is EditorEvent.AddText -> {
                val text = EditorObject.Text(
                    textId = UUID.randomUUID().toString(),
                    text = contentEvent.text,
                    offset = Offset(
                        x = contentEvent.x + (contentEvent.width / 2),
                        y = contentEvent.y + (contentEvent.height / 2)
                    ),
                    color = contentEvent.color ?: Color.Unspecified
                )
                _uiState.update {
                    it.copy(
                        drawingObjects = _uiState.value.drawingObjects.toMutableList().apply {
                            add(text)
                        }.toList(),
                        selectedTool = null
                    )
                }
            }

            is EditorEvent.BrushEvent -> {
                when (contentEvent.event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        val path = Path().apply {
                            moveTo(contentEvent.event.x, contentEvent.event.y)
                        }
                        _uiState.update {
                            it.copy(
                                currentDrawingPath = EditorObject.BrushPath(
                                    mutableStateOf(path),
                                    _uiState.value.brushConfiguration
                                )
                            )
                        }
                    }

                    MotionEvent.ACTION_MOVE -> {
                        val updatePath = brushPath.path.value.apply {
                            lineTo(contentEvent.event.x, contentEvent.event.y)
                        }

                        _uiState.update {
                            it.copy(
                                currentDrawingPath = EditorObject.BrushPath(
                                    mutableStateOf(updatePath),
                                    brushPath.brushConfiguration
                                )
                            )
                        }
                    }

                    MotionEvent.ACTION_UP -> {
                        brushPath.let { path ->
                            _uiState.update {
                                it.copy(
                                    drawingObjects = _uiState.value
                                        .drawingObjects.toMutableList().apply {
                                            add(path)
                                        }.toList(),
                                    currentDrawingPath = null
                                )
                            }
                        }
                    }
                }
            }

            EditorEvent.CloseEditor -> TODO()

            is EditorEvent.ToolSelected -> {
                _uiState.update {
                    it.copy(selectedTool = contentEvent.tool)
                }
            }

            is EditorEvent.TransformObject -> {
                val selectedObject = _uiState.value.drawingObjects.find {
                    it.id == contentEvent.id
                } as EditorObject.Text

                val scale = selectedObject.scale * contentEvent.scale

                val rotation = selectedObject.rotation + contentEvent.rotation

                val transformedOffset = contentEvent.offset.copy(
                    x = contentEvent.offset.x * scale,
                    y = contentEvent.offset.y * scale,
                ).rotateBy(contentEvent.rotation)

                _uiState.update {
                    it.copy(
                        drawingObjects = _uiState.value.drawingObjects.toMutableList().apply {
                            val index =
                                _uiState.value.drawingObjects.indexOf(selectedObject)

                            val offset = selectedObject.offset.plus(transformedOffset)

                            set(
                                index,
                                selectedObject.copy(
                                    rotation = rotation,
                                    scale = scale,
                                    offset = offset
                                )
                            )
                        }.toList()
                    )
                }
            }

            EditorEvent.Undo -> {
                _uiState.update {
                    it.copy(
                        drawingObjects = _uiState.value.drawingObjects.toMutableList().apply {
                            removeLast()
                        }.toList()
                    )
                }
            }

            EditorEvent.UnselectTool -> {
                _uiState.update {
                    it.copy(selectedTool = null)
                }
            }

            is EditorEvent.UpdateColorEvent -> {
                _uiState.update {
                    it.copy(
                        brushConfiguration = _uiState.value.brushConfiguration.copy(
                            color = contentEvent.color
                        )
                    )
                }
            }

            is EditorEvent.UpdateToolThickness -> {
                _uiState.update {
                    it.copy(
                        brushConfiguration = _uiState.value.brushConfiguration.copy(
                            thickness = contentEvent.thickness
                        )
                    )
                }
            }
        }
    }

    private fun Offset.rotateBy(angle: Float): Offset {
        val angleInRadians = angle * PI / 180

        return Offset(
            (x * cos(angleInRadians) - y * sin(angleInRadians)).toFloat(),
            (x * sin(angleInRadians) + y * cos(angleInRadians)).toFloat()
        )
    }
}
