package com.example.imageeditorm3.utils

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.imageeditorm3.model.data.EditorTool

@Composable
fun Tool(
    modifier: Modifier = Modifier,
    tool: EditorTool,
    onSelected: (EditorTool) -> Unit
) {
    IconButton(onClick = { onSelected(tool) }) {
        Icon(
            modifier = modifier.padding(8.dp),
            imageVector = tool.icon,
            contentDescription = stringResource(id = tool.description),
            tint = Color.White
        )
    }
}