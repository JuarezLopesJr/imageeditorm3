@file:OptIn(ExperimentalComposeUiApi::class)

package com.example.imageeditorm3.ui.screens

import androidx.compose.foundation.Canvas
import androidx.compose.runtime.Composable
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.StrokeJoin
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.input.pointer.pointerInteropFilter
import com.example.imageeditorm3.model.data.EditorEvent
import com.example.imageeditorm3.model.data.EditorObject
import com.example.imageeditorm3.model.data.EditorTool

@Composable
fun DrawingArea(
    modifier: Modifier = Modifier,
    selectedTool: EditorTool?,
    drawingObjects: List<EditorObject>,
    currentPath: EditorObject?,
    handleEvent: (EditorEvent) -> Unit,
) {
    Canvas(
        modifier = modifier.pointerInteropFilter { motionEvent ->
            if (selectedTool is EditorTool.Brush) {
                handleEvent(EditorEvent.BrushEvent(motionEvent))
            }
            true
        },
    ) {
        (drawingObjects + currentPath)
            .filterIsInstance<EditorObject.BrushPath>()
            .forEach { drawingObject ->
                drawPath(
                    path = drawingObject.path.value,
                    color = drawingObject.brushConfiguration.color,
                    style = Stroke(
                        drawingObject.brushConfiguration.thickness,
                        cap = StrokeCap.Round,
                        join = StrokeJoin.Round
                    )
                )
            }
    }
}