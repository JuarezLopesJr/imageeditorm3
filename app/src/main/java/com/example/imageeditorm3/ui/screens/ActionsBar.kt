package com.example.imageeditorm3.ui.screens

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Undo
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.imageeditorm3.R
import com.example.imageeditorm3.model.data.EditorEvent
import com.example.imageeditorm3.model.data.EditorObject
import com.example.imageeditorm3.model.data.EditorTool

@Composable
fun ActionsBar(
    modifier: Modifier = Modifier,
    selectedTool: EditorTool?,
    drawingObjects: List<EditorObject>?,
    handleEvent: (EditorEvent) -> Unit,
) {
    Surface(modifier = modifier, color = Color.Black.copy(alpha = 0.4f)) {
        when (selectedTool) {
            is EditorTool.Brush -> {
                Row(modifier = Modifier.fillMaxWidth()) {
                    if (!drawingObjects.isNullOrEmpty()) {
                        IconButton(onClick = { handleEvent(EditorEvent.Undo) }) {
                            Icon(
                                imageVector = Icons.Default.Undo,
                                contentDescription = stringResource(R.string.cd_undo),
                                tint = Color.White
                            )
                        }

                        Spacer(modifier = Modifier.weight(1f))

                        IconButton(onClick = { handleEvent(EditorEvent.UnselectTool) }) {
                            Icon(
                                imageVector = Icons.Default.Done,
                                contentDescription = stringResource(R.string.cd_done),
                                tint = Color.White
                            )
                        }
                    }

                }
            }

            is EditorTool.Emoji -> TODO()
            is EditorTool.Text -> TODO()
            null -> {
                EditorToolBar(
                    modifier = Modifier.fillMaxWidth(),
                    onToolSelected = {
                        handleEvent(EditorEvent.ToolSelected(it))
                    },
                    closeEditor = {
                        handleEvent(EditorEvent.CloseEditor)
                    }
                )
            }
        }

        /*
                if (selectedTool == null) {
                    EditorToolBar(
                        modifier = Modifier.fillMaxWidth(),
                        onToolSelected = {
                            handleEvent(EditorEvent.ToolSelected(it))
                        },
                        closeEditor = {
                            handleEvent(EditorEvent.CloseEditor)
                        }
                    )
                }
        */
    }
}