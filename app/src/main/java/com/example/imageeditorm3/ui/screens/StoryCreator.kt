@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.imageeditorm3.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Android
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import com.example.imageeditorm3.model.data.EditorEvent
import com.example.imageeditorm3.model.data.EditorState

@Composable
fun StoryCreator(
    modifier: Modifier = Modifier,
    state: EditorState,
    handleEvent: (EditorEvent) -> Unit,
) {
    BoxWithConstraints(
        modifier = modifier
            .fillMaxSize()
            .background(Color.Gray),
    ) {
        Image(
            modifier = Modifier.fillMaxSize(),
            imageVector = Icons.Default.Android,
            contentDescription = null,
            contentScale = ContentScale.Crop
        )

        ActionsBar(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.TopCenter),
            selectedTool = state.selectedTool,
            drawingObjects = state.drawingObjects,
            handleEvent = handleEvent
        )

        DrawingArea(
            selectedTool = state.selectedTool,
            drawingObjects = state.drawingObjects,
            currentPath = state.currentDrawingPath,
            handleEvent = handleEvent
        )
    }
}