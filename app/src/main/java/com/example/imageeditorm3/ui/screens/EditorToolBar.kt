package com.example.imageeditorm3.ui.screens

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.imageeditorm3.R
import com.example.imageeditorm3.model.data.EditorTool
import com.example.imageeditorm3.utils.Tool

@Composable
fun EditorToolBar(
    modifier: Modifier = Modifier,
    onToolSelected: (EditorTool) -> Unit,
    closeEditor: () -> Unit
) {
    Row(modifier = modifier, verticalAlignment = Alignment.CenterVertically) {
        IconButton(onClick = closeEditor) {
            Icon(
                imageVector = Icons.Default.Close,
                contentDescription = stringResource(R.string.cd_close_editor),
                tint = Color.White
            )
        }

        Spacer(modifier = Modifier.weight(1f))

        Tool(tool = EditorTool.Text(), onSelected = onToolSelected)
        Tool(tool = EditorTool.Brush(), onSelected = onToolSelected)
        Tool(tool = EditorTool.Emoji(), onSelected = onToolSelected)

    }
}