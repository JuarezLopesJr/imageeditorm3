package com.example.imageeditorm3.ui.screens

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.imageeditorm3.viewmodel.EditorViewModel

@Composable
fun Stories() {
    val viewModel = viewModel<EditorViewModel>()
    val state by viewModel.uiState.collectAsStateWithLifecycle()

    StoryCreator(state = state, handleEvent = {})
}