package com.example.imageeditorm3.model.data

import com.example.imageeditorm3.model.data.EditorObject.BrushConfiguration
import com.example.imageeditorm3.model.data.EditorObject.BrushPath

data class EditorState(
    val selectedTool: EditorTool? = null,
    val drawingObjects: List<EditorObject> = emptyList(),
    val currentDrawingPath: BrushPath? = null,
    val brushConfiguration: BrushConfiguration = BrushConfiguration(),
)
