package com.example.imageeditorm3.model.data

import android.view.MotionEvent
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp

sealed class EditorEvent {
    class ToolSelected(val tool: EditorTool) : EditorEvent()

    data object UnselectTool : EditorEvent()

    data object Undo : EditorEvent()

    data object CloseEditor : EditorEvent()

    class BrushEvent(val event: MotionEvent) : EditorEvent()

    class UpdateColorEvent(val color: Color) : EditorEvent()

    class UpdateToolThickness(val thickness: Float) : EditorEvent()

    class AddText(
        val x: Float,
        val y: Float,
        val width: Int,
        val height: Int,
        val text: String,
        val color: Color? = null,
    ) : EditorEvent()

    class TransformObject(
        val id: String,
        val offset: Offset,
        val rotation: Float = 1f,
        val scale: Float = 1f,
    ) : EditorEvent()
}
